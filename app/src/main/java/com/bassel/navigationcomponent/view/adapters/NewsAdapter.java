package com.bassel.navigationcomponent.view.adapters;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bassel.navigationcomponent.R;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsAdapterViewHolder> {
    @NonNull
    @Override
    public NewsAdapter.NewsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.NewsAdapterViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class NewsAdapterViewHolder extends RecyclerView.ViewHolder{
        AppCompatTextView tvTitle, tvDescription;
        AppCompatCheckBox checkFavorite;
        public NewsAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tvTitle = itemView.findViewById(R.id.tvTitle);
            this.tvDescription = itemView.findViewById(R.id.tvDescription);
            this.checkFavorite = itemView.findViewById(R.id.checkFavorite);
        }
    }
}
